FROM maven:3-jdk-8

# inspired by https://github.com/xwiki/xwiki-jenkins-agent

# Install VNC + Docker CE
RUN apt-get update && \
  apt-get --no-install-recommends -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    zip \
    bzip2 \
    wget \
    software-properties-common

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
# Note: We must install the same version of Docker that is installed on the CI agents since we share the same docker
# socker (docker on docker) and we want to reduce likelihood of issues.
RUN apt-get update && \
  apt-get --no-install-recommends -y install \
    xfce4 xfce4-goodies xfonts-base tightvncserver docker-ce && \
  rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install Firefox.
# Note 1: This won't be needed when we'll have all our functional tests use docker. However, as a transitional step,
# we should provide it, so that all agents can use this image to build XWiki fully.
# Note 2: We also install FF 32.0.1 for older branches still using Selenium 2.x
ENV FIREFOX_VERSION_1 69.0.3
ENV FIREFOX_DOWNLOAD_URL_1="https://download-installer.cdn.mozilla.net/pub/firefox/releases/$FIREFOX_VERSION_1/linux-x86_64/en-US/firefox-$FIREFOX_VERSION_1.tar.bz2"
ENV FIREFOX_VERSION_2 32.0.1
ENV FIREFOX_DOWNLOAD_URL_2="https://download-installer.cdn.mozilla.net/pub/firefox/releases/$FIREFOX_VERSION_2/linux-x86_64/en-US/firefox-$FIREFOX_VERSION_2.tar.bz2"
RUN apt-get update && \
  bzip2 && \
  apt-get --no-install-recommends -y install libasound2 && \
  rm -rf /var/lib/apt/lists/* /var/cache/apt/* && \
  wget --no-verbose -O /tmp/firefox1.tar.bz2 $FIREFOX_DOWNLOAD_URL_1 && \
  rm -rf /opt/firefox && \
  tar -C /opt -xjf /tmp/firefox1.tar.bz2 && \
  rm /tmp/firefox1.tar.bz2 && \
  mv /opt/firefox /opt/firefox-$FIREFOX_VERSION_1 && \
  ln -fs /opt/firefox-$FIREFOX_VERSION_1/firefox /usr/bin/firefox && \
  wget --no-verbose -O /tmp/firefox2.tar.bz2 $FIREFOX_DOWNLOAD_URL_2 && \
  tar -C /opt -xjf /tmp/firefox2.tar.bz2 && \
  rm /tmp/firefox2.tar.bz2 && \
  mv /opt/firefox /opt/firefox-$FIREFOX_VERSION_2 && \
  ln -fs /opt/firefox-$FIREFOX_VERSION_2 /usr/bin/firefox-$FIREFOX_VERSION_2


# Install the most recent version of Java8
RUN apt-get update && apt-get -y install openjdk-8-jdk openjdk-8-jre-headless

RUN groupadd -r build && useradd --no-log-init -r -m -g build builduser -G docker
USER builduser

WORKDIR /home/builduser

# Copy VNC config files
COPY --chown=builduser:build vnc/.Xauthority .Xauthority
COPY --chown=builduser:build vnc/.vnc .vnc

# Generate a password for XVNC
RUN echo "jenkins" | vncpasswd -f > .vnc/passwd

# This is important as otherwise vncserver requires a password when started
RUN chmod 0600 .vnc/passwd

ARG USER_HOME_DIR="/home/builduser"
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"
